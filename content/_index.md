---
title: "Keyso : A Language for Defining Key-Value Sets"
type: article
layout: simple
autonumbering: true
---

In this article, we introduce the \texttt{keyso} language. \texttt{keyso} is a
yaml-based configuration space description that is designed to be
human-readable and easy to write. The readability and ease of writing are
achieved by treating a configuration in a configuration space as a key-value
set and providing a syntax for defining sets of key value sets and set
operations on such sets of key value sets.

# A Quick Look

As a quick example, consider the following `yaml` snippet:

{{< listing label="example" caption="Example Syntax for keyso" >}}
{{< highlight yaml "linenos=table,anchorlinenos=true" >}}
_product:
  _product: # result { (a:1, b:3), (a:1, b:4) }
      - a: ["1"]
      - b: ["3", "4"]
  _difference: # result { (c:1, d:3), (c:2, d:3), (c:2, d:4) }
      - _minuend:
          _product: # result { (c:1, d:3), (c:1, d:4), (c:2, d:3), (c:2, d:4) }
           - c: ["1", "2"]
           - d: ["3", "4"]
      - _subtrahend:
          _union: # result { (c:1, d:4) }
           - c: 1
             d: 4
{{< /highlight >}}
{{< /listing >}}

The example in {{< listref "example" >}} defines a set of key-value sets with
the following representations

\begin{align}
S = \left( K_1 \times K_2 \right) \times \left((K_3 \times K_4) - K_5\right)
\end{align}

where
\begin{align*}
K_1 &= \\{ (a:1) \\} & K_2 = \\{ (b:3), (b:4) \\} \\\\
K_3 &= \\{ (c:1), (c:2) \\} & K_4 = \\{ (d:3), (d:4) \\\\
K_5 &= \\{ (c:1, d:4) \\}
\end{align*}

The set $S$ can be expanded into an explicit list of key-value sets as follows:

| No. | Combination | No. | Combination |
| --- | ----------- | --- | ----------- |
| 1 | `a: 1, b: 3, c: 1, d: 3` | 4 | `a: 1, b: 4, c: 1, d: 3` |
| 2 | `a: 1, b: 3, c: 2, d: 3` | 5 | `a: 1, b: 4, c: 2, d: 3` |
| 3 | `a: 1, b: 3, c: 2, d: 4` | 6 | `a: 1, b: 4, c: 2, d: 4` |

# Motivation

The motivation for this language comes from the need to define a configuration
space for various applications. Due to the combinatorial nature of the
configuration space, the configuration space can be very large and difficult to
describe. There is no escaping the complexity of the configuration space
used in the applications itself. However, one can at least aim to make minimize
the semantic overhead description of the configuration space, thereby, making
the description as simple or as complex as the configuration space itself.

The most natural language for describing a configuration space is an algebra of
sets. Simple matrix-style configuration spaces can be described using products
of sets. More complex configuration spaces can be described using unions,
intersections, and differences of sets. The `keyso` language aims to provide a
`yaml`-based language for such a description of set operations on key-value
sets in a human-readable and writable format.

# Theoretical Foundations

In this section, we establish the theoretical foundations of the `keyso`
language. `keyso` is a language over key-value sets, therefore we start the
description by defining the spaces from which the keys and values are drawn. We
then define a key-value pair, a key-value set and operations over them.
Finally, we define the concept of a configuration, a configuration space and
operations over them.

## Spaces for Keys and Values

keyso uses certain keywords to define the structure of the configuration.
Consequently, our sets of keys and values have to exclude these keywords. We define
the set of keywords $\calW$, the set of keys $\calK$ and the set of values $\calV$
as follows.




{{< definition title="Keyword" >}}
A keyword is a string from the set $\calW$ where the set $\calW$ is defined as
\begin{align}
  \calW =
  \begin{Bmatrix}
    \text{_literal}, &\text{_akvs}, &\text{_product}, &\text{_union},  \\
    \text{_intersection}, &\text{_difference}, &\text{_minuend}, &\text{_subtrahend}
  \end{Bmatrix}
\end{align}
{{< /definition >}}

{{< definition title="Key" >}}

  A key $k$ is any finite string that is not a keyword. That is
  \begin{align}
    \calK = \left\{ k \mid  k \text{ is a finite string }, k \not\in \calW \right\}
  \end{align}
{{< /definition >}}

Having defined the set of keys, a natural definition for the set of values
might have been an identical set of strings. However, in practice, we observe
the need to have an arbitrary JSON object as a value as well. Without special
treatment, including such a JSON object as a value would create ambiguity in
the language. To resolve this ambiguity, we define the a literal object as
follows:

{{< definition title="Literal Object" >}}
  A literal object is a JSON dictionary with a single special key `_literal`
  and the corresponding value being an arbitrary JSON object. The set of
  literal objects is denoted by $\calL$.


  An example of a literal object in `yaml` notation is shown below:


{{< listing label="literal" caption="Example of a Literal Object" >}}
{{< highlight yaml "linenos=table,anchorlinenos=true" >}}
_literal:
  a: 1
  b: [1, 2, 3]
{{< /highlight >}}
{{< /listing >}}
{{< /definition >}}


Literal objects are treated as indivisible units in the \texttt{keyso}
language and are not further parsed or decomposed for parsing.  With this
definition, we can now define the concept of a value in \texttt{keyso}.

{{< definition title="Value" >}}
  A value in \texttt{keyso} is either a finite string that is not a keyword or a
  literal object.  The set of values $\calV$ is defined as
  \begin{align}
    \calV = \left\{ v \mid  v \in \calK \cup \calL \right\}
  \end{align}
{{< /definition >}}

## Configuration and Configuration Spaces

Having defined the keys and values, we can now define the concept of a key-value pair and
a configuration.



{{< definition title="Key-Value Pair" >}}
  A key-value pair $p$ is defined by a $2$-tuple $(k, v)$ where $k$ is a key
  and $v$ is a value, that is $k\in \calK$  and $v \in \calV$.
{{< /definition >}}


{{< definition title="Type of a Configuration" >}}
    The type of a configuration $c$ is the set of keys that are present in the configuration.
  \begin{align}
    \calT(c) = \{k \mid \exists (k, v) \in c\}
  \end{align}

  Since the type of a configuration is a set, all set operations like union,
  intersection, etc. are defined on the type of a configuration as well.
{{< /definition >}}

{{< definition title="Partial Order on Types" >}}
  Given types $t_1$ and $t_2$, we define a relation $\leq$ on the set of types as follows:
  \begin{align}
    t_1 \leq t_2 \iff t_1 \subseteq t_2
  \end{align}

  The relation $\leq$ is a partial order on the set of types. Two types $t_1$
  and $t_2$ are said to be disjoint if their intersection is empty.
{{< /definition >}}

In practice, we rarely work with a single configuration. Instead, we work with
a set of configurations. Furthermore, we want to perform set operations like
union, intersection, cartesian product, etc. on these sets of such
configurations. However, it is desirable to restrict the set of configurations
such that all configurations in the set have the same set of keys, that is,
they have the same type. We define the concept of a configuration space to
capture this idea.

{{< definition title="Configuration Space" >}}
  Given a set of keys $K$, a configuration space $\calC(K)$ over the type $K$ is a set
  of all configuration $c$ such that the type of $c$ is $K$.
  \begin{align}
    \calC(K) = \left\{c \mid \calT(c) = K\right\}
  \end{align}
{{< /definition >}}

{{< theorem title="Properties of Configuration Spaces" >}}
    Restricting the set of configurations to a configuration space has the
    following desirable properties:

    <ol>
    <li>
    Closed Under Union: If we have sets $C_1 \subset \calC(K)$ and $C_2
      \subset \calC(K)$, then the union of the two sets is also in the
      configuration space.
      \begin{align}
        C_1 \cup C_2 \subset \calC(K)
      \end{align}
    </li>
    <li>
    Closed Under Intersection: If we have sets $C_1 \subset \calC(K)$ and $C_2
    \subset \calC(K)$, then the intersection of the two sets is also in the
    configuration space.
      \begin{align}
        C_1 \cap C_2 \subset \calC(K)
      \end{align}
    </li>

    <li> Closed under Difference: If we have sets $C_1 \subset \calC(K)$ and
      $C_2 \subset \calC(K)$, then the difference of the two sets is also in
      the configuration space.
      \begin{align}
        C_1 - C_2 \subset \calC(K)
      \end{align}
    </li>
    </ol>


{{< /theorem >}}

## Set Operations on Configuration Spaces


Given a set of configuration spaces $\\{\calC(K_i)\\}_{i\in \mathbb{N}}$, it is useful define
operations on configuration spaces. Such kind of operations are useful in
combining configurations from different configuration spaces. Before defining
such operations, it is beneficial to place some restrictions on the
configuration spaces that we are working with. For example, if we want to
combine two configurations from two different configuration spaces, it is
desirable that the two configuration spaces do not have any common keys,
otherwise the combination would be ambiguous. We define the concept of
coherence of types to capture this idea.


{{< definition title="Coherence of Types" >}}
  Consider two types $K_1$ and $K_2$ respectively. We define the coherence of
  the two types denoted by $\langle K_1, K_2\rangle$ as the intersection of the
  two types.
  \begin{align}
     \langle K_1, K_2\rangle = K_1 \cap K_2
  \end{align}

  Two types $K_1$ and $K_2$ are said to be coherent if $K_1 = K_2$, incoherent
  if $\langle K_1, K_2\rangle = \emptyset$ and partially coherent for
  everything in between. The coherence of two configurations or two
  configuration spaces is then defined as the coherence of their types.

  \begin{align}
    \langle c_1, c_2\rangle &= \langle \calT(c_1), \calT(c_2)\rangle  &
    \langle \calC(K_1), \calC(K_2)\rangle &= \langle K_1, K_2\rangle
  \end{align}
{{< /definition >}}

With the idea of coherence of types, we can now define operations like product
of configurations across configuration spaces.

{{< definition title="Product of Configurations and Configuration Spaces" >}}
  Given two incoherent configurations $c_1$ and $c_2$ (that is $\langle c_1,
  c_2\rangle = \emptyset$), the product of the two configurations is defined as
  \begin{align}
    c_1 \times c_2 =  c_1 \cup c_2
  \end{align}
 Similarly, given two configuration spaces $\calC(K_1)$ and $\calC(K_2)$ such that
  $\langle K_1, K_2\rangle = \emptyset$, the product of the two configuration
  spaces is defined as
  \begin{align}
    \calC(K_1) \times \calC(K_2) = \left\{c_1 \times c_2 \mid c_1 \in \calC(K_1) \text{ and } c_2 \in \calC(K_2)\right\}
  \end{align}
{{< /definition >}}




